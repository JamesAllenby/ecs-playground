#ifndef MESH_HPP
#define MESH_HPP

#include "Vertex.hpp"

struct Mesh
{
    std::vector<Vertex> vertices;
};

#endif