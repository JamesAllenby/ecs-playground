#ifndef QUEUEFAMILYINDICES_HPP
#define QUEUEFAMILYINDICES_HPP

#include <optional>
#include <cstdint>

struct QueueFamilyIndices
{
    std::optional<uint32_t> graphics, transfer, present;

    bool isComplete() const;
};

#endif