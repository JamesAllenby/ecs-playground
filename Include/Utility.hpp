#ifndef UTILITY_HPP
#define UTILITY_HPP

#include <vector>
#include <filesystem>

namespace Utility
{
    std::vector<std::byte> readFileBytes(std::filesystem::path path);
}

#endif