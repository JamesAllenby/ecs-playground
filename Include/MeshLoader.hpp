#ifndef MESHLOADER_HPP
#define MESHLOADER_HPP

#include "Mesh.hpp"

#include <vector>
#include <cstddef>

class MeshLoader
{
public:
    static Mesh loadObj(std::vector<const char*> data);
};

#endif