#ifndef APPLICATION_HPP
#define APPLICATION_HPP

#include "WindowSystem.hpp"
#include "RenderSystem.hpp"

#include <memory>
#include <entt/entt.hpp>

class Application
{
private:

    std::unique_ptr<WindowSystem> m_window_system;
    std::unique_ptr<RenderSystem> m_render_system;

public:

    Application();

    ~Application();

    void Run();

};

#endif