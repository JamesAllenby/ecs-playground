#ifndef RENDERSYSTEM_HPP
#define RENDERSYSTEM_HPP

#include "QueueFamilyIndices.hpp"

#include "vk_mem_alloc.h"
#include <cstdint>
#include <vector>
#include <queue>
#include <optional>

#include <vulkan/vulkan.hpp>
#include <SDL2/SDL.h>

struct DeviceQueues
{
    vk::Queue graphics, transfer, present;
};

class RenderSystem
{
private:
    /**
     * vk::Instance handle
     * 
     * Used to setup the render system class and is available externally
     * to create vk::SurfaceKHR handles
     */
    vk::Instance m_instance;

    /**
     * vk::SurfaceKHR handle
     * 
     * Provided by the windowing system and is used as the presentation target
     */
    vk::SurfaceKHR m_surface;

    /**
     * vk::PhysicalDevice handle
     * 
     * Used to create the logical device and to initialize certain components
     * such as the Vulkan Memory Allocator
     */
    vk::PhysicalDevice m_physical_device;

    /**
     * vk::Device handle
     * 
     * A foundation component of the render system. Used to create swapchains,
     * syncronisation primitives, memory allocations and more.
     */
    vk::Device m_device;

    /**
     * VmaAllocator handle
     * 
     * Used to allocate buffers and images efficiently
     */
    VmaAllocator m_allocator;


    QueueFamilyIndices m_queue_family_indices;
    DeviceQueues m_device_queues;

    vk::CommandPool m_command_pool;

    vk::SurfaceCapabilitiesKHR m_swapchain_capabilities;

    /**
     * vk::SurfaceFormatKHR struct
     * 
     * Stores the surface format used by the swapchain
     */
    vk::SurfaceFormatKHR m_swapchain_format;

    /**
     * vk::PresentModeKHR enum
     * 
     * Stores the presentation mode used by the swapchain
     */
    vk::PresentModeKHR m_swapchain_present_mode;

    /**
     * vk::SwapchainKHR handle
     * 
     * Contains all images that can be used to present to a window system
     * and handles presentation of images
     */
    vk::SwapchainKHR m_swapchain;

    /**
     *
     */
    std::vector<vk::Image> m_swapchain_images;

    /**
     *
     */
    std::vector<vk::ImageView> m_swapchain_image_views;

    /**
     *
     */
    std::vector<vk::Framebuffer> m_framebuffers;


    vk::RenderPass m_render_pass;


    vk::Pipeline m_graphics_pipeline;

    /**
     * vk::Semaphore handles
     * 
     * Used to signal when an image is available
     */
    std::vector<vk::Semaphore> m_image_available_semaphores;

    /**
     * vk::Semaphore handles
     * 
     * Used to signal when an image is ready to be presented
     */
    std::vector<vk::Semaphore> m_image_finished_semaphores;

    /**
     * vk::Fence handles for each swapchain image
     */
    std::vector<vk::Fence> m_fences;

    /**
     * vk::CommandBuffer handles for each swapchain image
     */
    std::vector<vk::CommandBuffer> m_command_buffers;

    /**
     * uint32_t frame counter
     * 
     * Incremented by one after every call to drawFrame
     */
    uint32_t m_frame_count = 0;

    /**
     * Get validation layers needed to debug Vulkan programs
     */
    std::vector<const char*> getValidationLayers() const;

    /**
     * Get the Vulkan device extensions needed for this application
     */
    std::vector<const char*> getRequiredDeviceExtensions() const;

    /**
     * Returns a QueueFamilyIndices with the supported indices on a physical device
     */
    QueueFamilyIndices getQueueFamilyIndicesFromPhysicalDevice(vk::PhysicalDevice physical_device) const;

    /**
     * Check if the physical device has all required features
     */
    bool isPhysicalDeviceSuitable(vk::PhysicalDevice physical_device) const;

    /**
     * Picks the most optimal swapchain surface format
     */
    vk::SurfaceFormatKHR pickSwapchainSurfaceFormat(const std::vector<vk::SurfaceFormatKHR>& surface_formats) const;

    /**
     * Picks the most optimal swapchain present mode
     */
    vk::PresentModeKHR pickSwapchainPresentMode(const std::vector<vk::PresentModeKHR>& present_modes) const;

    /**
     * Creates a shader module from a vector of SPIR-V code
     */
    vk::ShaderModule createShaderModule(const std::vector<std::byte>& code) const;

    /**
     * Initialises the physical device by picking the most suitable one
     */
    void initPhysicalDevice();

    /**
     * Initialises the logical device and obtains the queues
     */
    void initDevice();

    /**
     * Initialises the VMA (Vulkan Memory Allocator) library
     */
    void initVMA();

    /**
     * Initialises the command pool
     */
    void initCommandPool();

    /**
     * Creates the swapchain, stores the images and metadata about
     * the capabilities, format and presentation mode used
     */
    void createSwapchain();

    /**
     * Creates image views from the swapchain images stored from the
     * createSwapchain method
     */
    void createSwapchainImageViews();

    /**
     * Creates the render pass for rendering tasks
     */
    void createRenderPass();

    /**
     * Creates the graphics pipeline
     */
    void createGraphicsPipeline();

    /**
     * Generates framebuffers for use by the main render pass
     */
    void createFramebuffers();

    /**
     * Creates semaphores for each image available in the swapchain
     */
    void createSemaphores();

    /**
     * Creates fences for each image available in the swapchain
     */
    void createFences();

    /**
     * Creates a command buffer for each image available in the swapchain
     */
    void createCommandBuffers();

public:
    /**
     * Render System constructor
     * 
     * Performs a basic initialization of the Vulkan instance.
     * Call setup(vk::SurfaceKHR surface) after to use render system.
     */
    RenderSystem(
        std::string application_name, uint32_t application_version,
        std::string engine_name, uint32_t engine_version,
        std::vector<const char*> instance_extensions);

    /**
     * Render System destructor
     */
    ~RenderSystem();

    /**
     * Returns the vk::Instance associated with the render system
     */
    vk::Instance getInstance() const;

    /**
     * Setup the render system with the provided surface
     */
    void setup(const vk::SurfaceKHR& surface);

    /**
     * Queue a frame to be drawn and presented to surface provided in setup
     */
    void drawFrame();
};

#endif