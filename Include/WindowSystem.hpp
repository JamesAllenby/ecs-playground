#ifndef WINDOWSYSTEM_HPP
#define WINDOWSYSTEM_HPP

#include <string>
#include <vector>
#include <SDL2/SDL.h>
#include <vulkan/vulkan.hpp>

class WindowSystem
{
private:
    /**
     * Handle to the logical window
     */
    SDL_Window* m_window;
public:
    /**
     * Window System constructor
     */
    WindowSystem(const std::string& title, const int& width, const int& height);

    /**
     * Window System destructor
     */
    ~WindowSystem();

    /**
     * Returns a list of Vulkan instance extensions required to create a surface
     */
    std::vector<const char*> getInstanceExtensions() const;

    /**
     * Creates a Vulkan rendering surface for the window and returns it
     */
    vk::SurfaceKHR createSurface(const vk::Instance& instance);
};

#endif