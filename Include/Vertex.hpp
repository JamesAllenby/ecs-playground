#ifndef VERTEX_HPP
#define VERTEX_HPP

#include <glm/glm.hpp>
#include <vulkan/vulkan.hpp>

struct Vertex
{
    glm::vec3 position;

    /**
     * 
     */
    static vk::VertexInputBindingDescription getBindingDescription();

    /**
     * 
     */
    static std::vector<vk::VertexInputAttributeDescription> getAttributeDescription();
};

#endif