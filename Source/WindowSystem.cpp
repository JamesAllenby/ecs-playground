#include "WindowSystem.hpp"

#include <SDL2/SDL_vulkan.h>

WindowSystem::WindowSystem(const std::string& title, const int& width, const int& height)
{
    uint32_t flags = SDL_WINDOW_VULKAN;
    uint32_t xpos = SDL_WINDOWPOS_CENTERED;
    uint32_t ypos = SDL_WINDOWPOS_CENTERED;
    m_window = SDL_CreateWindow(title.c_str(), xpos, ypos, width, height, flags);
    if (!m_window)
    {
        std::string err = "Failed to create window: ";
        err.append(SDL_GetError());
        throw std::runtime_error(err);
    }
}

WindowSystem::~WindowSystem()
{
    SDL_DestroyWindow(m_window);
}

std::vector<const char*> WindowSystem::getInstanceExtensions() const
{
    unsigned int count = 0;
    if (!SDL_Vulkan_GetInstanceExtensions(m_window, &count, nullptr))
        throw std::runtime_error("Window System: Failed to query required instance extensions");
    std::vector<const char*> extensions(static_cast<size_t>(count));
    if (!SDL_Vulkan_GetInstanceExtensions(m_window, &count, extensions.data()))
        throw std::runtime_error("Window System: Failed to query required instance extensions");
    return extensions;
}

vk::SurfaceKHR WindowSystem::createSurface(const vk::Instance& instance)
{
    VkSurfaceKHR surface;
    if (!SDL_Vulkan_CreateSurface(m_window, instance, &surface))
        throw std::runtime_error("Window System: Failed to create Vulkan surface");
    return vk::SurfaceKHR(surface);
}