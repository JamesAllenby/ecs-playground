#include "Application.hpp"

#include "WindowSystem.hpp"
#include "RenderSystem.hpp"

#include <vector>

Application::Application()
{
    // App information
    std::string application_name = "ECS-Playground";
    uint32_t application_version = VK_MAKE_VERSION(0, 1, 0);
    std::string engine_name = "ECS-Playground";
    uint32_t engine_version = VK_MAKE_VERSION(0, 1, 0);

    // Window resolution
    uint32_t width = 800;
    uint32_t height = 600;

    m_window_system = std::make_unique<WindowSystem>(application_name, width, height);
    auto required_extensions = m_window_system->getInstanceExtensions();

    m_render_system = std::make_unique<RenderSystem>(
        application_name, application_version,
        engine_name, engine_version,
        required_extensions);

    auto surface = m_window_system->createSurface(m_render_system->getInstance());
    m_render_system->setup(surface);
}

Application::~Application()
{
}

void Application::Run()
{
    bool hasExit = false;
    while (!hasExit)
    {
        m_render_system->drawFrame();
    }
}