#include "QueueFamilyIndices.hpp"

bool QueueFamilyIndices::isComplete() const
{
    return graphics.has_value() && transfer.has_value() && present.has_value();
}