#include "Utility.hpp"

#include <vector>
#include <string>
#include <fstream>
#include <sstream>

std::vector<std::byte> Utility::readFileBytes(std::filesystem::path path)
{
    using std::ios;
    std::ifstream fd(path, ios::in | ios::binary | ios::ate);

    if (!fd.is_open())
        throw std::runtime_error("Failed to open file for reading");

    size_t size = fd.tellg();
    fd.seekg(0);

    std::vector<std::byte> contents(size);
    fd.read(reinterpret_cast<char*>(contents.data()), size);

    return contents;
}