#include "Vertex.hpp"

vk::VertexInputBindingDescription Vertex::getBindingDescription()
{
    return vk::VertexInputBindingDescription{
        0u,                          /* binding */
        sizeof(Vertex),              /* stride */
        vk::VertexInputRate::eVertex /* input rate */
    };
}

std::vector<vk::VertexInputAttributeDescription> Vertex::getAttributeDescription()
{
    return std::vector<vk::VertexInputAttributeDescription>{
        { /* position */
            0u,                           /* location */
            0u,                           /* binding */
            vk::Format::eR32G32B32Sfloat, /* format */
            offsetof(Vertex, position)    /* offset */
        }
    };
}