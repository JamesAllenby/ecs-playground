#include "RenderSystem.hpp"

#include "Vertex.hpp"
#include "Utility.hpp"

#include "vk_mem_alloc.h"
#include <set>
#include <iostream>
#include <string>
#include <SDL2/SDL.h>
#include <SDL2/SDL_vulkan.h>


std::vector<const char*> RenderSystem::getValidationLayers() const
{
#if NDEBUG
    return std::vector<const char*>{};
#else
    return std::vector<const char*>{
        "VK_LAYER_KHRONOS_validation"
    };
#endif
}

std::vector<const char*> RenderSystem::getRequiredDeviceExtensions() const
{
    return std::vector<const char*> {
        VK_KHR_SWAPCHAIN_EXTENSION_NAME
    };
}

QueueFamilyIndices RenderSystem::getQueueFamilyIndicesFromPhysicalDevice(vk::PhysicalDevice physical_device) const
{
    QueueFamilyIndices qfi;
    auto queue_family_properties = physical_device.getQueueFamilyProperties();
    for (size_t idx = 0; idx < queue_family_properties.size(); idx++)
    {
        // Get queue family property based on index
        const auto& queue_family_property = queue_family_properties.at(idx);
        uint32_t uidx = static_cast<uint32_t>(idx);
        // Check graphics queue support
        if (queue_family_property.queueFlags & vk::QueueFlagBits::eGraphics && !qfi.graphics.has_value())
            qfi.graphics = uidx;
        // Check transfer queue support
        if (queue_family_property.queueFlags & vk::QueueFlagBits::eTransfer && !qfi.transfer.has_value())
            qfi.transfer = uidx;
        // Check presentation queue support
        if (physical_device.getSurfaceSupportKHR(uidx, m_surface))
            qfi.present = uidx;
        // Break loop when queue family index is complete
        if (qfi.isComplete())
            break;
    }
    return qfi;
}

bool RenderSystem::isPhysicalDeviceSuitable(vk::PhysicalDevice physical_device) const
{
    QueueFamilyIndices qfi = getQueueFamilyIndicesFromPhysicalDevice(physical_device);
    if (!qfi.isComplete())
        return false;
    return true;
}

vk::SurfaceFormatKHR RenderSystem::pickSwapchainSurfaceFormat(const std::vector<vk::SurfaceFormatKHR>& formats) const
{
    for (const auto& format : formats)
        if (format.format == vk::Format::eR8G8B8A8Srgb && format.colorSpace == vk::ColorSpaceKHR::eSrgbNonlinear)
            return format;
    return formats.front();
}

vk::PresentModeKHR RenderSystem::pickSwapchainPresentMode(const std::vector<vk::PresentModeKHR>& present_modes) const
{
    for (const auto& present_mode : present_modes)
        if (present_mode == vk::PresentModeKHR::eMailbox)
            return present_mode;
    return vk::PresentModeKHR::eFifo;
}

vk::ShaderModule RenderSystem::createShaderModule(const std::vector<std::byte>& code) const
{
    vk::ShaderModuleCreateInfo create_info{};
    create_info.setPCode(reinterpret_cast<const uint32_t*>(code.data()));
    create_info.setCodeSize(code.size());
    return m_device.createShaderModule(create_info);
}

void RenderSystem::initPhysicalDevice()
{
    if (!m_instance)
        throw std::runtime_error("Failed to init physical device: Missing instance");
    auto physical_devices = m_instance.enumeratePhysicalDevices();
    if (physical_devices.empty())
        throw std::runtime_error("Failed to init physical device: No compatible device");
    for (const auto& physical_device : physical_devices)
    {
        if (isPhysicalDeviceSuitable(physical_device))
        {
            m_physical_device = physical_device;
            m_queue_family_indices = getQueueFamilyIndicesFromPhysicalDevice(physical_device);
            break;
        }
    }
    if (!m_physical_device)
        throw std::runtime_error("Failed to init physical device: Missing queue support");
}

void RenderSystem::initDevice()
{
    if (!m_physical_device)
        throw std::runtime_error("Failed to init device: Missing physical device");

    // Set of queue indices for logical device creation
    std::set<uint32_t> queue_indices{
        m_queue_family_indices.graphics.value(),
        m_queue_family_indices.transfer.value(),
        m_queue_family_indices.present.value()
    };

    float queue_priority = 1.0f;
    std::vector<vk::DeviceQueueCreateInfo> queue_create_infos{};
    for (const auto& queue_index : queue_indices)
    {
        vk::DeviceQueueCreateInfo queue_create_info{};
        queue_create_info.setQueueCount(1)
            .setQueueFamilyIndex(queue_index)
            .setQueuePriorities(queue_priority);
        queue_create_infos.push_back(queue_create_info);
    }

    std::vector<const char*> device_extensions{};
    auto required_extensions = getRequiredDeviceExtensions();
    device_extensions.insert(device_extensions.cend(), required_extensions.cbegin(), required_extensions.cend());

    std::vector<const char*> device_layers{};
    auto validation_layers = getValidationLayers();
    device_layers.insert(device_layers.cend(), validation_layers.cbegin(), validation_layers.cend());

    vk::DeviceCreateInfo device_create_info{};
    device_create_info.setQueueCreateInfos(queue_create_infos)
        .setPEnabledExtensionNames(device_extensions)
        .setPEnabledLayerNames(device_layers);
    m_device = m_physical_device.createDevice(device_create_info);

    m_device_queues.graphics = m_device.getQueue(m_queue_family_indices.graphics.value(), 0);
    m_device_queues.transfer = m_device.getQueue(m_queue_family_indices.transfer.value(), 0);
    m_device_queues.present = m_device.getQueue(m_queue_family_indices.present.value(), 0);
}

void RenderSystem::initVMA()
{
    if (!m_instance || !m_physical_device || !m_device)
        throw std::runtime_error("Failed to init VMA: Missing instance, physical device or device");
    VmaAllocatorCreateInfo create_info{};
    create_info.vulkanApiVersion = VK_API_VERSION_1_0;
    create_info.instance = m_instance;
    create_info.physicalDevice = m_physical_device;
    create_info.device = m_device;
    auto result = static_cast<vk::Result>(vmaCreateAllocator(&create_info, &m_allocator));
    if (result != vk::Result::eSuccess)
        throw std::runtime_error("Failed to init VMA: Unknown error");
}

void RenderSystem::initCommandPool()
{
    if (!m_device)
        throw std::runtime_error("Failed to init command pool: Missing device");
    m_device.destroyCommandPool(m_command_pool);
    vk::CommandPoolCreateInfo create_info{};
    create_info.setFlags(vk::CommandPoolCreateFlagBits::eResetCommandBuffer);
    create_info.setQueueFamilyIndex(m_queue_family_indices.graphics.value());
    m_command_pool = m_device.createCommandPool(create_info);
}

void RenderSystem::createSwapchain()
{
    // Get swapchain/surface properties from physical device
    m_swapchain_capabilities = m_physical_device.getSurfaceCapabilitiesKHR(m_surface);

    // Get most optimal swapchain surface format and present mode
    auto surface_formats = m_physical_device.getSurfaceFormatsKHR(m_surface);
    auto present_modes = m_physical_device.getSurfacePresentModesKHR(m_surface);
    m_swapchain_format = pickSwapchainSurfaceFormat(surface_formats);
    m_swapchain_present_mode = pickSwapchainPresentMode(present_modes);

    // Get minimum image count for swapchain
    uint32_t image_count = m_swapchain_capabilities.minImageCount + 1;
    if (m_swapchain_capabilities.maxImageCount > 0)
        image_count = std::clamp(image_count, m_swapchain_capabilities.minImageCount, m_swapchain_capabilities.maxImageCount);

    // Create swapchain
    vk::SwapchainCreateInfoKHR create_info;
    create_info.setSurface(m_surface)
        .setMinImageCount(image_count)
        .setImageFormat(m_swapchain_format.format)
        .setImageColorSpace(m_swapchain_format.colorSpace)
        .setImageExtent(m_swapchain_capabilities.currentExtent)
        .setImageArrayLayers(1)
        .setImageUsage(vk::ImageUsageFlagBits::eColorAttachment)
        .setImageSharingMode(vk::SharingMode::eExclusive)
        .setPreTransform(m_swapchain_capabilities.currentTransform)
        .setCompositeAlpha(vk::CompositeAlphaFlagBitsKHR::eOpaque)
        .setPresentMode(m_swapchain_present_mode)
        .setClipped(false)
        .setOldSwapchain(m_swapchain);

    m_swapchain = m_device.createSwapchainKHR(create_info);
    m_swapchain_images = m_device.getSwapchainImagesKHR(m_swapchain);
}

void RenderSystem::createSwapchainImageViews()
{
    // Reset swapchain image views vector
    for (const auto& image_view : m_swapchain_image_views)
        m_device.destroyImageView(image_view);
    m_swapchain_image_views.clear();
    m_swapchain_image_views.reserve(m_swapchain_images.size());

    // Create image views for every swapchain image
    for (const auto& image : m_swapchain_images)
    {
        vk::ComponentMapping component_mapping;
        component_mapping.setR(vk::ComponentSwizzle::eR)
            .setG(vk::ComponentSwizzle::eG)
            .setB(vk::ComponentSwizzle::eB)
            .setA(vk::ComponentSwizzle::eA);
        vk::ImageSubresourceRange subresource_range;
        subresource_range.setAspectMask(vk::ImageAspectFlagBits::eColor)
            .setBaseArrayLayer(0)
            .setBaseMipLevel(0)
            .setLayerCount(1)
            .setLevelCount(1);

        vk::ImageViewCreateInfo create_info;
        create_info.setImage(image)
            .setViewType(vk::ImageViewType::e2D)
            .setFormat(m_swapchain_format.format)
            .setComponents(component_mapping)
            .setSubresourceRange(subresource_range);

        m_swapchain_image_views.push_back(m_device.createImageView(create_info));
    }
}

void RenderSystem::createRenderPass()
{
    vk::AttachmentDescription color_attachment{};
    color_attachment.setFormat(m_swapchain_format.format)
        .setSamples(vk::SampleCountFlagBits::e1)
        .setLoadOp(vk::AttachmentLoadOp::eClear)
        .setStoreOp(vk::AttachmentStoreOp::eStore)
        .setStencilLoadOp(vk::AttachmentLoadOp::eDontCare)
        .setStencilStoreOp(vk::AttachmentStoreOp::eDontCare)
        .setInitialLayout(vk::ImageLayout::eUndefined)
        .setFinalLayout(vk::ImageLayout::ePresentSrcKHR);

    vk::AttachmentReference color_attachment_ref{};
    color_attachment_ref.setAttachment(0).setLayout(vk::ImageLayout::eColorAttachmentOptimal);

    vk::SubpassDescription subpass_description{};
    subpass_description.setColorAttachments(color_attachment_ref)
        .setPipelineBindPoint(vk::PipelineBindPoint::eGraphics);

    vk::RenderPassCreateInfo create_info{};
    create_info.setAttachments(color_attachment).setSubpasses(subpass_description);

    m_render_pass = m_device.createRenderPass(create_info);
}

void RenderSystem::createFramebuffers()
{
    const auto& surface_extent = m_swapchain_capabilities.currentExtent;
    for (const auto& image_view : m_swapchain_image_views)
    {
        std::array<vk::ImageView, 1> attachment{ image_view };
        vk::FramebufferCreateInfo create_info{};
        create_info.setAttachments(attachment)
            .setWidth(surface_extent.width)
            .setHeight(surface_extent.height)
            .setRenderPass(m_render_pass)
            .setLayers(1);
        m_framebuffers.push_back(m_device.createFramebuffer(create_info));
    }
}

void RenderSystem::createGraphicsPipeline()
{
    // Load vertex and fragment shaders
    auto vert_shader_code = Utility::readFileBytes("Shader/Default.vert.spv");
    auto frag_shader_code = Utility::readFileBytes("Shader/Default.frag.spv");
    auto vert_shader_module = createShaderModule(vert_shader_code);
    auto frag_shader_module = createShaderModule(frag_shader_code);

    vk::PipelineShaderStageCreateInfo vert_stage{};
    vert_stage.setStage(vk::ShaderStageFlagBits::eVertex);
    vert_stage.setModule(vert_shader_module);
    vert_stage.setPName("main");

    vk::PipelineShaderStageCreateInfo frag_stage{};
    frag_stage.setStage(vk::ShaderStageFlagBits::eFragment);
    frag_stage.setModule(frag_shader_module);
    frag_stage.setPName("main");

    std::vector<vk::PipelineShaderStageCreateInfo> shader_stages{ vert_stage, frag_stage };

    // Vertex input state create info
    vk::PipelineVertexInputStateCreateInfo vertex_input_state{};
    std::vector<vk::VertexInputBindingDescription> binding_descriptions{ Vertex::getBindingDescription() };
    std::vector<vk::VertexInputAttributeDescription> attribute_descriptions{ Vertex::getAttributeDescription() };
    vertex_input_state.setVertexBindingDescriptions(binding_descriptions);
    vertex_input_state.setVertexAttributeDescriptions(attribute_descriptions);

    // Input assembly state create info
    vk::PipelineInputAssemblyStateCreateInfo input_assembly_state{};
    input_assembly_state.setTopology(vk::PrimitiveTopology::eTriangleList);

    // Viewport state create info
    const auto& surface_extent = m_swapchain_capabilities.currentExtent;
    vk::Rect2D scissor{
        {0, 0}, /* offset */
        {0, 0}  /* extent */
    };
    vk::Viewport viewport{
        0.0f,                         /* x */
        0.0f,                         /* y */
        float(surface_extent.width),  /* width */
        float(surface_extent.height), /* height */
        0.0f,                         /* min depth */
        1.0f                          /* max depth */
    };
    vk::PipelineViewportStateCreateInfo viewport_state{};
    viewport_state.setScissors(scissor);
    viewport_state.setViewports(viewport);

    // Rasterization state create info
    vk::PipelineRasterizationStateCreateInfo rasterization_state{};
    rasterization_state.setDepthClampEnable(false)
        .setRasterizerDiscardEnable(false)
        .setPolygonMode(vk::PolygonMode::eFill)
        .setCullMode(vk::CullModeFlagBits::eBack)
        .setFrontFace(vk::FrontFace::eClockwise)
        .setLineWidth(1.0f)
        .setDepthBiasEnable(false);

    // Multisample state create info
    vk::PipelineMultisampleStateCreateInfo multisample_state{};
    multisample_state.setRasterizationSamples(vk::SampleCountFlagBits::e1);

    // Color blend attachment state create info
    vk::PipelineColorBlendAttachmentState color_attachment{};
    color_attachment.setColorWriteMask(
        vk::ColorComponentFlagBits::eR | vk::ColorComponentFlagBits::eG |
        vk::ColorComponentFlagBits::eB | vk::ColorComponentFlagBits::eA);
    vk::PipelineColorBlendStateCreateInfo color_blend_state{};
    color_blend_state.setLogicOpEnable(false)
        .setAttachments(color_attachment);

    // Layout create info
    vk::PipelineLayoutCreateInfo layout_create_info{};
    auto layout = m_device.createPipelineLayout(layout_create_info);

    vk::GraphicsPipelineCreateInfo graphics_pipeline_create_info{};
    graphics_pipeline_create_info
        .setStages(shader_stages)
        .setPVertexInputState(&vertex_input_state)
        .setPInputAssemblyState(&input_assembly_state)
        .setPViewportState(&viewport_state)
        .setPRasterizationState(&rasterization_state)
        .setPMultisampleState(&multisample_state)
        .setPColorBlendState(&color_blend_state)
        .setLayout(layout)
        .setRenderPass(m_render_pass)
        .setSubpass(0);

    auto graphics_pipeline = m_device.createGraphicsPipeline(nullptr, graphics_pipeline_create_info);
    if (graphics_pipeline.result != vk::Result::eSuccess)
        throw std::runtime_error("Failed to create graphics pipeline");
    m_graphics_pipeline = graphics_pipeline.value;
}

void RenderSystem::createSemaphores()
{
    if (!m_device)
        throw std::runtime_error("Failed to create semaphores: Missing device");

    auto destroy_semaphores = [this](const vk::Semaphore& s) { m_device.destroySemaphore(s); };
    std::for_each(m_image_available_semaphores.cbegin(), m_image_available_semaphores.cend(), destroy_semaphores);
    std::for_each(m_image_finished_semaphores.cbegin(), m_image_finished_semaphores.cend(), destroy_semaphores);
    m_image_available_semaphores.clear();
    m_image_finished_semaphores.clear();

    for (size_t i = 0; i < m_swapchain_images.size(); i++)
    {
        m_image_available_semaphores.push_back(m_device.createSemaphore({}));
        m_image_finished_semaphores.push_back(m_device.createSemaphore({}));
    }
}

void RenderSystem::createFences()
{
    if (!m_device)
        throw std::runtime_error("Failed to create fences: Missing device");
    // Destroy existing fences
    for (auto& fence : m_fences)
        m_device.destroyFence(fence);
    m_fences.clear();
    m_fences.reserve(m_swapchain_images.size());
    // Create new fences
    for (size_t i = 0; i < m_swapchain_images.size(); i++)
        m_fences.push_back(m_device.createFence({}));
}

void RenderSystem::createCommandBuffers()
{
    if (!m_device)
        throw std::runtime_error("Failed to create command buffers");
    // Free existing command buffers
    if (!m_command_buffers.empty())
    {
        m_device.freeCommandBuffers(m_command_pool, m_command_buffers);
        m_command_buffers.clear();
    }
    // Allocate command buffers from command pool
    vk::CommandBufferAllocateInfo allocate_info{};
    allocate_info.setCommandPool(m_command_pool)
        .setCommandBufferCount(static_cast<uint32_t>(m_swapchain_images.size()))
        .setLevel(vk::CommandBufferLevel::ePrimary);
    m_command_buffers = m_device.allocateCommandBuffers(allocate_info);
}

RenderSystem::RenderSystem(
    std::string application_name, uint32_t application_version,
    std::string engine_name, uint32_t engine_version,
    std::vector<const char*> instance_extensions)
{
    // Generate instance extension list
    std::vector<const char*> enabled_extensions{};
    enabled_extensions.insert(enabled_extensions.cend(), instance_extensions.cbegin(), instance_extensions.cend());

    std::vector<const char*> enabled_layers{};
    auto validation_layers = getValidationLayers();
    enabled_layers.insert(enabled_layers.cend(), validation_layers.cbegin(), validation_layers.cend());

    // Application info
    vk::ApplicationInfo app_info{};
    app_info.setPApplicationName(application_name.c_str())
        .setApplicationVersion(application_version)
        .setPEngineName(engine_name.c_str())
        .setEngineVersion(engine_version)
        .setApiVersion(VK_API_VERSION_1_0);

    // Instance create info
    vk::InstanceCreateInfo create_info{};
    create_info.setPApplicationInfo(&app_info)
        .setPEnabledExtensionNames(enabled_extensions)
        .setPEnabledLayerNames(enabled_layers);

    m_instance = vk::createInstance(create_info);
}

RenderSystem::~RenderSystem()
{
}

vk::Instance RenderSystem::getInstance() const
{
    return m_instance;
}

void RenderSystem::setup(const vk::SurfaceKHR& surface)
{
    m_surface = surface;

    initPhysicalDevice();
    initDevice();
    initVMA();
    initCommandPool();

    createSwapchain();
    createSwapchainImageViews();
    createSemaphores();
    createFences();
    createCommandBuffers();

    createRenderPass();
    createGraphicsPipeline();
    createFramebuffers();
}

void RenderSystem::drawFrame()
{
    // Get an image semaphore corresponding with the current frame count
    const auto& image_available_semaphore = m_image_available_semaphores.at(m_frame_count % m_image_available_semaphores.size());
    const auto& image_finished_semaphore = m_image_finished_semaphores.at(m_frame_count % m_image_finished_semaphores.size());

    // Acquire next available swapchain image
    auto image_index = m_device.acquireNextImageKHR(m_swapchain, UINT32_MAX, image_available_semaphore, nullptr);
    if (image_index.result != vk::Result::eSuccess)
    {
        std::cerr << "Failed to obtain next swapchain image, skipping frame" << std::endl;
        return;
    }

    // Retrieve command buffer and framebuffer for this image index
    auto& command_buffer = m_command_buffers.at(static_cast<size_t>(image_index.value));
    auto& frame_buffer = m_framebuffers.at(static_cast<size_t>(image_index.value));

    //
    auto surface_extent = m_swapchain_capabilities.currentExtent;
    vk::ClearColorValue clear_color{};
    clear_color.setFloat32({ 0.0f, 0.0f, 0.0f, 1.0f });
    std::vector<vk::ClearValue> clear_values{ clear_color };

    vk::RenderPassBeginInfo rp_info{};
    rp_info.renderPass = m_render_pass;
    rp_info.framebuffer = frame_buffer;
    rp_info.renderArea = vk::Rect2D{ { 0, 0 }, surface_extent };
    rp_info.setClearValues(clear_values);

    // Record command buffer
    vk::CommandBufferBeginInfo begin_info{};
    command_buffer.begin(begin_info);
    command_buffer.bindPipeline(vk::PipelineBindPoint::eGraphics, m_graphics_pipeline);
    command_buffer.beginRenderPass(rp_info, vk::SubpassContents::eInline);



    command_buffer.endRenderPass();
    command_buffer.end();
    // Finish command buffer

    // Prepare to submit command buffer
    std::vector<vk::CommandBuffer> submit_cbs = { command_buffer };                     // Command buffers to submit
    std::vector<vk::Semaphore> submit_wait_semaphores = { image_available_semaphore };  // Semaphores to wait before execution
    std::vector<vk::Semaphore> submit_finish_semaphores = { image_finished_semaphore }; // Semaphores to signal after execution
    vk::PipelineStageFlags submit_pipeline_stages = vk::PipelineStageFlagBits::eAllCommands;

    vk::SubmitInfo submit_info{};
    submit_info.setCommandBuffers(submit_cbs);
    submit_info.setWaitSemaphores(submit_wait_semaphores);
    submit_info.setSignalSemaphores(submit_finish_semaphores);
    submit_info.setWaitDstStageMask(submit_pipeline_stages);

    // Submit buffer
    std::array<vk::SubmitInfo, 1> submit_infos = { submit_info };
    m_device_queues.graphics.submit(submit_infos);

    // Present image
    vk::PresentInfoKHR present_info{};
    present_info.setWaitSemaphores(submit_finish_semaphores);
    present_info.setSwapchains(m_swapchain);
    present_info.setImageIndices(image_index.value);
    if (m_device_queues.present.presentKHR(present_info) != vk::Result::eSuccess)
        std::cout << "Failed to present frame index " << image_index.value << std::endl;
}